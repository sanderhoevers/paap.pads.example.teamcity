package demo.businessrule;

import com.planonsoftware.platform.backend.businessrule.v3.IBusinessRule;
import com.planonsoftware.platform.backend.businessrule.v3.IBusinessRuleContext;
import com.planonsoftware.platform.backend.data.v1.IBusinessObject;
import com.planonsoftware.platform.backend.data.v1.IStringField;

public class CheckSurname implements IBusinessRule {
    private static final String FIELD_SURNAME = "LastName";
    private static final int MESSAGE_ID = 123;
    @Override
    public void execute(IBusinessObject newBO, IBusinessObject oldBO, IBusinessRuleContext context) {
        if (isInsertOrRelevantChange(newBO)) {
            checkAndUpdateSurname(newBO.getStringFieldByName(FIELD_SURNAME), context);
        }
    }
    private boolean isInsertOrRelevantChange(IBusinessObject newBO) {
        return newBO.isNew() || newBO.getFieldByName(FIELD_SURNAME).isChanged();
    }
    private void checkAndUpdateSurname(IStringField surnameField, IBusinessRuleContext context) {
        if (hasInvalidCapitalization(surnameField.getValueAsString())) {
            updateCaptialization(surnameField, context);
        }
    }
    private boolean hasInvalidCapitalization(String surname) {
        return Character.isLowerCase(surname.charAt(0));
    }
    private void updateCaptialization(IStringField surnameField, IBusinessRuleContext context) {
        if (context.getErrorHandlingService().isConfirmed(MESSAGE_ID)) {//user responded to confirmation
            if (context.getErrorHandlingService().isConfirmedPositive(MESSAGE_ID)) {//user approved fixing
                fixFirstCharacter(surnameField);
            }
         // else save as is
        } else {
            context.getErrorHandlingService().addConfirmation(123, "Surname doesn't start with a capital.\nDo you want to fix this?\n");
        }
    }
    private void fixFirstCharacter(IStringField surnameField) {
        String fixedSurName = capitalizeFirstCharacter(surnameField.getValueAsString());
        surnameField.setValue(fixedSurName);
    }
    private String capitalizeFirstCharacter(String text) {
        return text.substring(0, 1).toUpperCase() + text.substring(1);
    }
}