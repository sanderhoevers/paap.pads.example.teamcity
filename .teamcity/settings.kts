import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.gradle
import jetbrains.buildServer.configs.kotlin.v2019_2.triggers.vcs
import jetbrains.buildServer.configs.kotlin.v2019_2.vcs.GitVcsRoot

/*
The settings script is an entry point for defining a TeamCity
project hierarchy. The script should contain a single call to the
project() function with a Project instance or an init function as
an argument.

VcsRoots, BuildTypes, Templates, and subprojects can be
registered inside the project using the vcsRoot(), buildType(),
template(), and subProject() methods respectively.

To debug settings scripts in command-line, run the

    mvnDebug org.jetbrains.teamcity:teamcity-configs-maven-plugin:generate

command and attach your debugger to the port 8000.

To debug in IntelliJ Idea, open the 'Maven Projects' tool window (View
-> Tool Windows -> Maven Projects), find the generate task node
(Plugins -> teamcity-configs -> teamcity-configs:generate), the
'Debug' option is available in the context menu for the task.
*/

version = "2020.2"

project {
//    params {
//        password("pass-to-bucket", "credentialsJSON:4dc857e4-a90c-4ae5-9cf8-86967a0a83d1")
//    }

    vcsRoot(MyVcsRoot)

    buildType(Build)
    buildType(TestSlow)
    buildType(TestFast)
    buildType(Validate)
    buildType(Deploy)


    sequential {
        buildType(Build)
        parallel {
            buildType(TestSlow)
            buildType(TestFast)
        }
        buildType(Validate)
        buildType(Deploy)
    }
}

object MyVcsRoot : GitVcsRoot({
    name = DslContext.getParameter("vcsName")
    url = DslContext.getParameter("vcsUrl")
    branch = DslContext.getParameter("vcsBranch", "refs/heads/master")

//    authMethod = password{
//                userName = DslContext.getParameter("user-to-bucket")
//                password = DslContext.getParameter("credentialsJSON:4dc857e4-a90c-4ae5-9cf8-86967a0a83d1")}
})

object Build : BuildType({
    name = "Build"

    vcs {
        root(MyVcsRoot)
    }

    steps {
        gradle {
            tasks = ":printHello"
        }
    }

    triggers {
//        vcs {
//        }
    }
})

object TestSlow : BuildType({
    name = "TestSlow"

    vcs {
        root(MyVcsRoot)
    }

    steps {
        gradle {
            tasks = ":printHello"
        }
    }

})

object TestFast : BuildType({
    name = "TestFast"

    vcs {
        root(MyVcsRoot)
    }

    steps {
        gradle {
            tasks = ":printHello"
        }
    }

})

object Validate : BuildType({
    name = "Validate"

    vcs {
        root(MyVcsRoot)
    }

    steps {
        gradle {
            tasks = ":printHello"
        }
    }

//    dependencies{
//        snapshot(Build){}
//    }

})

object Deploy : BuildType({
    name = "Deploy"
    type = Type.DEPLOYMENT

    vcs {
        root(MyVcsRoot)
    }

    dependencies{
        snapshot(Build){}
    }

    steps {
        gradle {
            tasks = ":printHello"
        }
    }

})
